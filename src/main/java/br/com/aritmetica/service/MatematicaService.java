package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n: entradaDTO.getNumeros()) {
            if(n < 0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "So é permitido números naturais ");
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n: entradaDTO.getNumeros()) {
            numero -= n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int numero = 0;
        numero = entradaDTO.getNumeros().get(0)/entradaDTO.getNumeros().get(1);
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int numero = 0;
        numero = entradaDTO.getNumeros().get(0)*entradaDTO.getNumeros().get(1);
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }


}
