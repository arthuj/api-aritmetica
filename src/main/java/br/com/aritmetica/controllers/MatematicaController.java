package br.com.aritmetica.controllers;


import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {
        validarNatural(entradaDTO);
        validarQuantidadeMinima(entradaDTO);
        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO) {
        validarNatural(entradaDTO);
        validarQuantidadeMinima(entradaDTO);
        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO) {
        validarNatural(entradaDTO);
        validarQuantidadeMinima(entradaDTO);
        validarQuantidadeMaxima(entradaDTO);
        if(entradaDTO.getNumeros().get(0) < entradaDTO.getNumeros().get(1))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O primeiro número deve ser maior que o segundo");
        }
        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;
    }

    @PutMapping("multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO) {
        validarNatural(entradaDTO);
        validarQuantidadeMinima(entradaDTO);
        validarQuantidadeMaxima(entradaDTO);
        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }

    public void validarNatural(EntradaDTO entradaDTO) {
        for (int n : entradaDTO.getNumeros()) {
            if (n < 0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é permitido números naturais");
        }
    }

    public void validarQuantidadeMinima(EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros");
        }
    }

    public void validarQuantidadeMaxima(EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() > 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é permitido 2 números nessa operação");
        }
    }
}
